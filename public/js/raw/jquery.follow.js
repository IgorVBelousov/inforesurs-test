(function ($) {
  $.fn.follow = function () {
    var $this = $(this);

    function follow_mouse(obj) {
      $('body').mousemove(function (e, h) {
        $(obj).offset({top: e.pageY, left: e.pageX});
        $(obj).click(function () {
          $('body').off('mousemove')
        })
      })
    }

    $this.mouseover(function (e, h) {
      follow_mouse(this);
    });
  };
})(jQuery);