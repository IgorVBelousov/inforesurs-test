<?php
/**
 * Ресайз и наложение ватермарки на прозрачную анимированную гифку.
 */

namespace App\Utils;

use Imagick;

/**
 * Class GifModifier.
 *
 * Ресайз и наложение ватермарки на прозрачную анимированную гифку
 */
class GifModifier
{
    /**
     * Ресайз и наложение ватермарки на прозрачную анимированную гифку.
     *
     * @param string $filename          Путь к исходному gif-файлу
     * @param int    $width             Новый размер по горизонтали
     * @param int    $height            Новый размер по вертикале
     * @param string $watermarkFilename Путь к водному знаку. gif или png
     * @param int    $watermarkX        Позиция водного знака по горизонтали
     * @param int    $watermarkY        Позиция водного знака по вертикали
     * @param string $outFilename       Путь для записи результата
     */
    public function modify(string $filename, int $width, int $height, string $watermarkFilename, int $watermarkX, int $watermarkY, string $outFilename)
    {
        $watermark = new Imagick();
        $watermark->readImage($watermarkFilename);

        $gifFile = new Imagick($filename);

        $fileFormat = $gifFile->getImageFormat();

        if ('GIF' == $fileFormat) {
            $frames = $gifFile->coalesceImages();

            foreach ($frames as $frame) {
                $frame->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
                $frame->compositeImage($watermark, Imagick::COMPOSITE_OVER, $watermarkX, $watermarkY);
            }

            $out_file = $frames->deconstructImages();
            $out_file->writeImages($outFilename, true);

            $out_file->clear();
        }

        $gifFile->clear();
        $watermark->clear();
    }
}
