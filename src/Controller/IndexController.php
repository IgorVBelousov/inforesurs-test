<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Utils\GifModifier;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index_page", methods={"GET"})
     */
    public function index()
    {
        return $this->render('index.html.twig');
    }

    /**
     * @Route("/follow", name="follow_page", methods={"GET"})
     */
    public function follow()
    {
        return $this->render('follow.html.twig');
    }

    /**
     * @Route("/gif_modify", name="gif_page", methods={"GET"})
     */
    public function gifModify()
    {
        return $this->render('gif_modify.html.twig', [
            'result' => false,
        ]);
    }

    /**
     * @Route("/gif_modify/process", name="gif_process_page", methods={"GET"})
     */
    public function gifModifyProcess()
    {
        $img_dir = dirname(dirname(__DIR__)).'/public/img/';
        $gifModify = new GifModifier();
        $gifModify->modify($img_dir.'original.gif', 180, 146, $img_dir.'PHP.png', 36, 52, $img_dir.'out_180_146.gif');
        $gifModify->modify($img_dir.'original.gif', 300, 243, $img_dir.'circle_php.png', 88, 152, $img_dir.'out_300_243.gif');

        return $this->redirectToRoute('gif_result_page');
    }

    /**
     * @Route("/gif_modify/results", name="gif_result_page", methods={"GET"})
     */
    public function gifModifyResults()
    {
        return $this->render('gif_modify.html.twig', [
            'result' => true,
            'images' => [
                [
                    'x' => 180,
                    'y' => 146,
                ],
                [
                    'x' => 300,
                    'y' => 243,
                ],
            ],
        ]);
    }
}
