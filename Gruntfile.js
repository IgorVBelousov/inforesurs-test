module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    uglify: {
      "jquery.follow": {
        src: "public/js/raw/jquery.follow.js",
        dest: "public/js/jquery.follow.min.js"
      }

    }

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-internal');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-uglify-es');

  grunt.registerTask('js', ['uglify']);

  grunt.registerTask('default', ['js']);
};
